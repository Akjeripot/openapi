swagger: "2.0"
info:
  version: 1.0.0
  title: User-Address API
  description: The Address Consumer API RESTful web service is a secure service requiring an authentication token. This service API calls the eBusiness User Data Service (UDS) service and transforms the response from SOAP to JSON. The service provides the functionality to 1) return a list of addresses, 2) create a new address, 3) update an address, and 4) delete an address.
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : ""
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
produces:
  - application/json
paths:
  /mycare/v1.0/address:
    get:
      description: |
            Returns a list of addresses.  
      operationId: getaddress
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'

      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema: 
            $ref: "#/definitions/AddressListResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
    put:
      description: |
            User requests creating or updating a single address. If there is an existing address with the passed type and label, the address will be updated; otherwise, a new address will be created.
      operationId: putaddress
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: X-type
          in: header
          description: User address type
          default: work
          required: true
          type: string
        - name: X-label
          in: header
          description: User address label
          required: true
          default: workplace address
          type: string
        - name: X-street1
          in: header
          description: User address street
          required: true
          default: 1375 Peachtree St NE
          type: string
        - name: X-city
          in: header
          description: User address city
          required: true
          default: Atlanta
          type: string
        - name: X-state
          in: header
          description: User address state
          required: true
          default: GA
          type: string
        - name: X-postalCode
          in: header
          description: User address postal code
          required: true
          default: 30309
          type: integer
        - name: X-sourceSystem
          in: header
          description: User address source system
          required: true
          default: KP
          type: string
        - name: X-street2
          in: header
          description: User street2 address source system
          required: false
          type: string
        - name: X-country
          in: header
          description: User address country
          required: false
          type: string

      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema: 
            $ref: "#/definitions/UpdatePersonDataResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'

    delete:
      description: |
           Deletes user address 
      operationId: deleteaddress
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: X-type
          in: header
          description: User address type
          default: work
          required: true
          type: string
        - name: X-label
          in: header
          description: User address label
          required: true
          default: workplace address
          type: string
        - name: X-sourceSystem
          in: header
          description: User address source system
          required: true
          default: KP
          type: string

      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema: 
            $ref: "#/definitions/DeletePersonDataResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'

definitions:
    DeletePersonDataResponse:
          type: object
          properties:
            DeletePersonDataResponse:
              type: object
              properties:
                executionContext:
                  type: 'null'
                success:
                  type: boolean
              required:
              - executionContext
              - success
          required:
          - DeletePersonDataResponse

    AddressListResponse:
          type: object
          properties:
            AddressListResponse:
              type: object
              properties:
                executionContext:
                  type: 'null'
                addresses:
                  type: array
                  items:
                    type: object
                    properties:
                      type:
                        type: string
                      label:
                        type: string
                      street1:
                        type: string
                      street2:
                        type: string
                      city:
                        type: string
                      state:
                        type: string
                      postalCode:
                        type: integer
                      sourceSystem:
                        type: string
                    required:
                    - type
                    - label
                    - street1
                    - street2
                    - city
                    - state
                    - postalCode
                    - sourceSystem
              required:
              - executionContext
              - addresses
          required:
          - AddressListResponse

    UpdatePersonDataResponse:
          type: object
          properties:
            UpdatePersonDataResponse:
              type: object
              properties:
                executionContext:
                  type: 'null'
                success:
                  type: boolean
              required:
              - executionContext
              - success
          required:
          - UpdatePersonDataResponse

    503ErrorRespSchema:
        type: object
        properties:
          executionContext:
            type: object
            properties:
              errors:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              warnings:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              infos:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
            required:
            - errors
            - warnings
            - infos
        required:
        - executionContext

parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: 'Device OS Version. For example 5.1.' 
    default: '5.1'
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
    
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  405Error:
      description: "Method not supported"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"