swagger: '2.0'
info:
  version: "1.0"
  title: Payment API
  description : "Generic API to authorize payment and/or post the payment to backend service. It can void the payment if post payment results in failure. Calls PPS to authorize and/or void the payment. Calls PostPayment operation of EpicPatientAccessGSPatientServices2012 service for processing medical billing payments"
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
schemes:
        - https
externalDocs:
  description: "`API Wiki Link`"
  url : 
    "http://wiki-tech.kp.org/mediawiki/index.php/AxwayAPIs/MedicalBilling/PPS_One_Time_ACH_2.0
    http://wiki-tech.kp.org/mediawiki/index.php/AxwayAPIs/MedicalBilling/PPS_Payment_2.0"
host: xjzxqws011fx.dta.kp.org:8080
# Describe your paths here
paths:
  /mycare/accounting/v1.0/oneTimePayment:
    post:
      description: |
        The One Time ACH Payment operation allows a client to accept bank account details (identified by account/routing number), and authorize a charge against that account.
      tags:
        - One Time Payment
      produces:
        - application/json
      parameters:
        - name: X-apiKey
          in: header
          description: VAPI Key specific to the consumer, for e.g. kaisermobil93908784817875726966
          default: kprwd65766367497853935616
          required: true
          type: string
        - name: X-appName
          in: header
          description: Application name i.e. kp‐mobile_v2.0, KP Mobile
          default: RWD
          required: true
          type: string
        - name: X-useragenttype
          in: header
          description: Type of agent. For example iPhone5, Samsung etc.
          default: iphone4
          required: true
          type: string
        - name: X-useragentcategory
          in: header
          description: I = iOS, A = Android, M = mobile web
          default: I
          required: true
          type: string
        - name: X-osversion
          in: header
          description: Version of the device OS i.e. 5.1
          default: '5.1'
          required: true
          type: string
        - name: ssosession
          in: header
          description: ssosession header received from sign-on service. Required for authorization, Either ssoseesion header or ObSSOCookie is required
          default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
          required: true
          type: string
        - name: ObSSOCookie
          in: header
          description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
          required: false
          type: string
        - name: SessionToken
          in: header
          description: SSO Token. Received from payment profile response. Required for CSRF checks
          required: true
          type: string
        - name: Cookie
          in: header
          description: Obssosession+LTPAToken2 - Either ssosession header or ObSSOCookie is required. Required for CSRF decrypting payment token.
          required: true
          type: string
        - name: X-browserVersion
          in: header
          description: Browser Version. 
          required: false
          type: string
        - name: X-flashVersion
          in: header
          description: Flash Version. 
          required: false
          type: string
        - name: body
          in: body
          description: request for one time payment process
          required: true
          schema: 
            $ref: "#/definitions/requestOneTimePayment"
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            SessionToken:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/transactionOneTimePayment"
        206:
          description: "Partial response because one or more backend services are down/not responding."
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID corresponding to the failed MRN/s"
            SessionToken:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/transactionOneTimePayment"
        401:
          description: "Unauthorized"
        403:
          description: "Access Denied. User is not entitled to access the requested API."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        404:
          description: "Resource Not Found"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        412:
          description: "Pre-condition failed. One or more required headers, params are not present in the request."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            SessionToken:
              type: string
              description: "SSO Token"
        500:
          description: "Internal Error in Axway"
        501:
          description: "Internal Error in Axway"
        503:
          description: "System error. Something went wrong while processing the request. Service unavailable."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
          schema:
                $ref : "#/definitions/503ErrorRespSchema"              

  /mycare/accounting/v1.0/payment:
    post:
      description: |
        The payment service handles all aspects of the Medical Bill Pay payments from charging. the credit to posting back to Epic as well as many checks and balances in the middle.
      tags:
        - Payment
      produces:
        - application/json
      parameters:
        - name: X-apiKey
          in: header
          description: VAPI Key specific to the consumer, for e.g. kaisermobil93908784817875726966
          default: kprwd65766367497853935616
          required: true
          type: string
        - name: X-appName
          in: header
          description: Application name i.e. kp‐mobile_v2.0, KP Mobile
          default: RWD
          required: true
          type: string
        - name: X-useragenttype
          in: header
          description: Type of agent. For example iPhone5, Samsung etc.
          default: iphone4
          required: true
          type: string
        - name: X-useragentcategory
          in: header
          description: I = iOS, A = Android, M = mobile web
          default: I
          required: true
          type: string
        - name: X-osversion
          in: header
          description: Version of the device OS i.e. 5.1
          default: '5.1'
          required: true
          type: string
        - name: ssosession
          in: header
          description: ssosession header received from sign-on service. Required for authorization, Either ssoseesion header or ObSSOCookie is required
          default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
          required: true
          type: string
        - name: ObSSOCookie
          in: header
          description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
          required: false
          type: string
        - name: SessionToken
          in: header
          description: SSO Token. Received from payment profile response. Required for CSRF checks
          required: true
          type: string
        - name: Cookie
          in: header
          description: Obssosession+LTPAToken2 - Either ssosession header or ObSSOCookie is required. Required for CSRF decrypting payment token.
          required: true
          type: string
        - name: X-browserVersion
          in: header
          description: Browser Version. 
          required: false
          type: string
        - name: X-flashVersion
          in: header
          description: Flash Version. 
          required: false
          type: string
        - name: body
          in: body
          description: request for one time payment process
          required: true
          schema: 
            $ref: "#/definitions/requestPayment"
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            SessionToken:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/transactionPayment"
        206:
          description: "Partial response because one or more backend services are down/not responding."
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID corresponding to the failed MRN/s"
            SessionToken:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/transactionPayment"
        401:
          description: "Unauthorized"
        403:
          description: "Access Denied. User is not entitled to access the requested API."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        404:
          description: "Resource Not Found"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
        412:
          description: "Pre-condition failed. One or more required headers, params are not present in the request."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            SessionToken:
              type: string
              description: "SSO Token"
        500:
          description: "Internal Error in Axway"
        501:
          description: "Internal Error in Axway"
        503:
          description: "System error. Something went wrong while processing the request. Service unavailable."
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
          schema:
                $ref : "#/definitions/503ErrorRespSchema"              

definitions:
  requestOneTimePayment:
      type: object
      properties:
        intent:
          type: string
          default: authorize
        type:
          type: string
          default: Medical billing
        typeCode:
          type: string
          default: MB
        transactionInfo:
          type: object
          properties:
            transaction:
              type: array
              items:
                type: object
                properties:
                  money:
                    type: object
                    properties:
                      amount:
                        type: string
                        default: '2.00'
                      currency:
                        type: string
                        default: USD
                    required:
                    - amount
                    - currency
                  gurantorTransactionInfo:
                    type: object
                    properties:
                      desc:
                        type: string
                        default: Professional billing
                      guarantorAccountId:
                        type: string
                        default: '51250369'
                      billingSystem:
                        type: string
                        default: PB
                      deploymentId:
                        type: string
                        default: urn:kp:hcotnw2
                    required:
                    - desc
                    - guarantorAccountId
                    - billingSystem
                    - deploymentId
                required:
                - money
                - gurantorTransactionInfo
            duplicateOverride:
              type: string
              default: 'true'
            termsConditionVersion:
              type: string
              default: '1'
            termsConditionLastModified:
              type: string
              default: 06/26/2009 11:35 PM
          required:
          - transaction
          - duplicateOverride
          - termsConditionVersion
          - termsConditionLastModified
        payer:
          type: object
          properties:
            checkMicrData:
              type: string
              default: 121000358T0015743947A
            paymentMethod:
              type: string
              default: CK
            healthCapability:
              type: string
              default: N
            emailId:
              type: string
              default: stephencurry@gsw.com
            fName:
              type: string
              default: onetimepayment
            lName:
              type: string
              default: test
            correlationId:
              type: string
              default: 78c410a0-34e1-11e6-9398-fcf847da76ea
          required:
          - checkMicrData
          - paymentMethod
          - healthCapability
          - emailId
          - fName
          - lName
          - correlationId
      required:
      - intent
      - type
      - typeCode
      - transactionInfo
      - payer
    
  transactionOneTimePayment:
    type: object
    properties:
      authResponseReason:
        type: string
      error:
        type: object
        properties:
          errorDesc:
            type: string
          errorCode:
            type: string
          errorLevel:
            type: string
        required:
        - errorDesc
        - errorCode
        - errorLevel
      authorizationAmount:
        type: integer
      remainingBalance:
        type: string
      poeUniqueId:
        type: string
      xmlns:
        type: string
      authTime:
        type: integer
      authorizationCode:
        type: string
      bankNetInfo:
        type: string
      statusCode:
        type: integer
      transactionType:
        type: string
      stan:
        type: integer
      merchantId:
        type: string
      transactionNumber:
        type: integer
      hostActionCode:
        type: string
      authResponseCode:
        type: integer
      xmlns:mes:
        type: string
      authDate:
        type: integer
    required:
    - authResponseReason
    - error
    - authorizationAmount
    - remainingBalance
    - poeUniqueId
    - xmlns
    - authTime
    - authorizationCode
    - bankNetInfo
    - statusCode
    - transactionType
    - stan
    - merchantId
    - transactionNumber
    - hostActionCode
    - authResponseCode
    - xmlns:mes
    - authDate
    
  requestPayment:
      type: object
      properties:
        intent:
          type: string
          default: authorize
        type:
          type: string
          default: Medical billing
        typeCode:
          type: string
          default: MB
        transactionInfo:
          type: object
          properties:
            transaction:
              type: array
              items:
                type: object
                properties:
                  money:
                    type: object
                    properties:
                      amount:
                        type: string
                        default: '2.00'
                      currency:
                        type: string
                        default: USD
                    required:
                    - amount
                    - currency
                  gurantorTransactionInfo:
                    type: object
                    properties:
                      desc:
                        type: string
                        default: Professional billing
                      guarantorAccountId:
                        type: string
                        default: '51250369'
                      billingSystem:
                        type: string
                        default: PB
                      deploymentId:
                        type: string
                        default: urn:kp:hcotnw2
                    required:
                    - desc
                    - guarantorAccountId
                    - billingSystem
                    - deploymentId
                required:
                - money
                - gurantorTransactionInfo
            duplicateOverride:
              type: string
              default: 'true'
            termsConditionVersion:
              type: string
              default: '1'
            termsConditionLastModified:
              type: string
              default: 06/26/2009 11:35 PM
          required:
          - transaction
          - duplicateOverride
          - termsConditionVersion
          - termsConditionLastModified
        payer:
          type: object
          properties:
            checkMicrData:
              type: string
              default: 121000358T0015743947A
            paymentMethod:
              type: string
              default: CK
            healthCapability:
              type: string
              default: N
            emailId:
              type: string
              default: stephencurry@gsw.com
            fName:
              type: string
              default: onetimepayment
            lName:
              type: string
              default: test
            correlationId:
              type: string
              default: 78c410a0-34e1-11e6-9398-fcf847da76ea
          required:
          - checkMicrData
          - paymentMethod
          - healthCapability
          - emailId
          - fName
          - lName
          - correlationId
      required:
      - intent
      - type
      - typeCode
      - transactionInfo
      - payer
    
  transactionPayment:
    type: object
    properties:
      authResponseReason:
        type: string
      error:
        type: object
        properties:
          errorDesc:
            type: string
          errorCode:
            type: string
          errorLevel:
            type: string
        required:
        - errorDesc
        - errorCode
        - errorLevel
      authorizationAmount:
        type: integer
      remainingBalance:
        type: string
      poeUniqueId:
        type: string
      xmlns:
        type: string
      authTime:
        type: integer
      authorizationCode:
        type: string
      bankNetInfo:
        type: string
      statusCode:
        type: integer
      transactionType:
        type: string
      stan:
        type: integer
      merchantId:
        type: string
      transactionNumber:
        type: integer
      hostActionCode:
        type: string
      authResponseCode:
        type: integer
      xmlns:mes:
        type: string
      authDate:
        type: integer
    required:
    - authResponseReason
    - error
    - authorizationAmount
    - remainingBalance
    - poeUniqueId
    - xmlns
    - authTime
    - authorizationCode
    - bankNetInfo
    - statusCode
    - transactionType
    - stan
    - merchantId
    - transactionNumber
    - hostActionCode
    - authResponseCode
    - xmlns:mes
    - authDate
  503ErrorRespSchema:
        type: object
        properties:
          executionContext:
            type: object
            properties:
              errors:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              warnings:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              infos:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
            required:
            - errors
            - warnings
            - infos
        required:
        - executionContext
    