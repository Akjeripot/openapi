swagger: "2.0"
info:
  version: 1.0.0
  title: Pharmacy RxOrder API
  description: The Medication Order Service Consumer API RESTful web service is a secure service requiring an authentication token. This service API calls the eBusiness MOS (SOAP) service and transforms the response from SOAP to JSON. The service returns a member’s active medications, their pharmacy prescription status, pharmacy member information, and the ability to place pharmacy refill orders.
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : "https://sites.sp.kp.org/projects/csdt/APIIS/integrationServices/Shared%20Documents/APIs/MOS%20Consumer%20API%20Document.doc"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
consumes:
  - application/json
produces:
  - application/json
paths:
  /mycare/v1.0/placeorder:
    put:
      description: |
          The service returns business feature entitlements for a subject or a subject with a delegate.      
      operationId: putPlaceorder
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        
        - name: X-binaryToken
          in: header
          description: The X-binaryToken header name is expected whenever we a pciToken name/value pair is being passed in the JSON body. If the binaryToken is not present when the pciToken is passed, a 412 (PreCondition Failed) will occur.
          required: false
          type: string
        
        - name : putPlaceorderRequest
          in: body
          description: "Placeorder Request"
          required : true
          schema:
            $ref : "#/definitions/putPlaceorderRequest"
        
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/putPlaceorderResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
definitions:
  putPlaceorderRequest:
      type: object
      properties:
        PlaceOrder:
          type: object
          properties:
            deliveryMethod:
              type: string
              default: MAIL
            emailAddress:
              type: string
              default: xyz@abc.com
            region:
              type: string
              default: MRN
            specialInstructions:
              type: string
              default: KEEP IT IN COOL PLACE
            creditCard:
              type: object
              properties:
                CreditCard:
                  type: object
                  properties:
                    lastFourDigits:
                      type: integer
                      default: 1001
                    expirationDate:
                      type: string
                      default: 12/15
                    type:
                      type: string
                      default: AMEX
                    cardHolderName:
                      type: object
                      properties:
                        firstName:
                          type: string
                          default: MARK
                        lastName:
                          type: string
                          default: SMITH
                        middleName:
                          type: string
                          default: A
                        nameSuffix:
                          type: string
                          default: MR
                      required:
                      - firstName
                      - lastName
                      - middleName
                      - nameSuffix
                    usage:
                      type: string
                      default: PERMANENT
                  required:
                  - lastFourDigits
                  - expirationDate
                  - type
                  - cardHolderName
                  - usage
              required:
              - CreditCard
            sourceApplication:
              type: string
              default: kponline
            transactionControlReference:
              type: integer
              default: 1234
            masterOrder:
              type: object
              properties:
                memberName:
                  type: object
                  properties:
                    firstName:
                      type: string
                      default: MARK
                    lastName:
                      type: string
                      default: SMITH
                    middleName:
                      type: string
                      default: A
                    nameSuffix:
                      type: string
                      default: MR
                  required:
                  - firstName
                  - lastName
                  - middleName
                  - nameSuffix
                address:
                  type: object
                  properties:
                    streetAddress1:
                      type: string
                      default: 278 SANTA MONICA BLVD
                    city:
                      type: string
                      default: BEVERLY HILLS
                    state:
                      type: string
                      default: CA
                    zipCode:
                      type: integer
                      default: 90211
                  required:
                  - streetAddress1
                  - city
                  - state
                  - zipCode
                businessPhone:
                  type: object
                  properties:
                    areaCode:
                      type: integer
                      default: 310
                    number:
                      type: integer
                      default: 5551316
                  required:
                  - areaCode
                  - number
                homePhone:
                  type: object
                  properties:
                    areaCode:
                      type: integer
                      default: 310
                    number:
                      type: integer
                      default: 5551316
                  required:
                  - areaCode
                  - number
                mrn:
                  type: integer
                  default: 13914009
                rxNumbers:
                  type: array
                  items:
                    type: object
                    properties:
                      rxNumber:
                        type: integer
                        default: 63146829
                      nhinId:
                        type: integer
                        default: 0
                      autoFillEnrollRx:
                        type: boolean
                        default: false
                      responseCode:
                        type: string
                        default: '00'
                      pharmacyId:
                        type: integer
                        default: 50
                      pickUpLocation:
                        type: string
                        default: PHARMACY
                    required:
                    - rxNumber
                    - nhinId
                    - autoFillEnrollRx
                    - responseCode
                    - pharmacyId
                    - pickUpLocation
              required:
              - memberName
              - address
              - businessPhone
              - homePhone
              - mrn
              - rxNumbers
            subOrders:
              type: array
              items:
                type: object
                properties:
                  memberName:
                    type: object
                    properties:
                      firstName:
                        type: string
                        default: SMITHS
                      lastName:
                        type: string
                        default: WIFE
                      middleName:
                        type: string
                        default: R
                      nameSuffix:
                        type: string
                        default: MRS
                    required:
                    - firstName
                    - lastName
                    - middleName
                    - nameSuffix
                  address:
                    type: object
                    properties:
                      streetAddress1:
                        type: string
                        default: AAAA
                      streetAddress2:
                        type: string
                        default: BBB
                      city:
                        type: string
                        default: FREMONT
                      state:
                        type: string
                        default: CA
                      zipCode:
                        type: integer
                        default: 94040
                      singleUse:
                        type: boolean
                        default: false
                    required:
                    - streetAddress1
                    - streetAddress2
                    - city
                    - state
                    - zipCode
                    - singleUse
                  mrn:
                    type: integer
                    default: 13914006
                  rxNumbers:
                    type: array
                    items:
                      type: object
                      properties:
                        rxNumber:
                          type: integer
                          default: 63146829
                        nhinId:
                          type: integer
                          default: 0
                        autoFillEnrollRx:
                          type: boolean
                          default: false
                        responseCode:
                          type: string
                          default: '00'
                        pharmacyId:
                          type: integer
                          default: 50
                        pickUpLocation:
                          type: string
                          default: PHARMACY
                      required:
                      - rxNumber
                      - nhinId
                      - autoFillEnrollRx
                      - responseCode
                      - pharmacyId
                      - pickUpLocation
                required:
                - memberName
                - address
                - mrn
                - rxNumbers
            regionalHost:
              type: string
              default: M
          required:
          - deliveryMethod
          - emailAddress
          - region
          - specialInstructions
          - creditCard
          - sourceApplication
          - transactionControlReference
          - masterOrder
          - subOrders
          - regionalHost
      required:
      - PlaceOrder
  putPlaceorderResponse:
      type: object
      properties:
        status:
          type: string
          default: success
      required:
      - status

  412ErrorRespSchema:      
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors      
  503ErrorRespSchema:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: Device OS Version. For example 5.1. 
    default: 5.1.
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
    
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      schema:
          $ref : "#/definitions/412ErrorRespSchema"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"
