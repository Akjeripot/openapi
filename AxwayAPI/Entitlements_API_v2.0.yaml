swagger: "2.0"
info:
  version: 2.0.0
  title: Entitlements API
  description: The Entitlements Consumer API RESTful web service is a secure service requiring an authentication token. This service API calls the eBusiness Entitlement (SOAP) service and transforms the response from SOAP to JSON. The service returns business feature entitlements for a subject or a subject with a delegate.
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : "http://wiki-tech.kp.org/mediawiki/index.php/Entitlements_API_2.0"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /mycare/v2.0
consumes:
  - application/json
produces:
  - application/json
paths:
  /entitlements:
    get:
      description: |
          The service returns business feature entitlements for a subject or a subject with a delegate.      
      operationId: getEntitlements
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/entitlementsResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
definitions:
  entitlementsResponse:
      type: object
      properties:
        Entitlements:
          type: object
          properties:
            executionContext:
              type: 'null'
              default: 
            AuthDecisions:
              type: array
              items:
                type: object
                properties:
                  Decision:
                    type: string
                    default: D
                  Status:
                    type: integer
                    default: 1
                  Id:
                    type: integer
                    default: 619
                required:
                - Decision
                - Status
                - Id
          required:
          - executionContext
          - AuthDecisions
      required:
      - Entitlements

  412ErrorRespSchema:      
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors      
  503ErrorRespSchema:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: Device OS Version. For example 5.1. 
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description: API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.

                                Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w  
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: true
    type: string
    
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      schema:
          $ref : "#/definitions/412ErrorRespSchema"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"
