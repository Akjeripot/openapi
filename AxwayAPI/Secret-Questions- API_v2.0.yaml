swagger: '2.0'
info:
  version: "1.0"
  title: Secret Questions  API
  description : "The Secret Questions Consumer API RESTful web service is a secure service requiring an authentication token. This service API calls the eBusiness Authorization Service and transforms the response from SOAP to JSON. The service provides the functionality to 1) return a list of active secret questions, and 2) persist answers to three secret questions for the logged in user"
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
schemes:
        - https
externalDocs:
  description: "`API Wiki Link`"
  url : "http://wiki-tech.kp.org/mediawiki/index.php/Secret_Questions_API"
host: xjzxqws011fx.dta.kp.org:8080
basePath: /

# Describe your paths here
paths:
  /care/v2.0/secretquestions:
    get:
      description: |
        User requests a list of all active secret questions. There are multiple groups of questions returned, and each group will be represented as an array of questions.
      produces:
        - application/json
      parameters:
        - name: X-appVersion
          in: header
          description: This is used to query the database.
          required: true
          type: string
        - name: X-apiKey
          in: header
          description: VAPI Key specific to the consumer, for e.g. kaisermobil93908784817875726966
          default: kprwd65766367497853935616
          required: true
          type: string
        - name: X-appName
          in: header
          description: Application name i.e. kp‐mobile_v2.0, KP Mobile
          default: RWD
          required: true
          type: string
        - name: X-useragenttype
          in: header
          description: Type of agent. For example iPhone5, Samsung etc.
          default: iphone4
          required: true
          type: string
        - name: X-useragentcategory
          in: header
          description: I = iOS, A = Android, M = mobile web
          default: I
          required: true
          type: string
        - name: X-osversion
          in: header
          description: Version of the device OS i.e. 5.1
          default: '5.1'
          required: true
          type: string
        - name: ssosession
          in: header
          description: ssosession header received from sign-on service. Required for authorization, Either ssoseesion header or ObSSOCookie is required
          default: 'CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh'
          required: true
          type: string
        - name: ObSSOCookie
          in: header
          description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
          required: false
          type: string

      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/Secret Questions"
        400:
          description: "Bad Request"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        401:
          description: "Unauthorized"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        412:
          description: "Precondition Failed"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        503:
          description: "Referenced Backend Service is not available/System Error"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
          schema:
                $ref : "#/definitions/503ErrorRespSchema"              
              
        404:
          description: "Resource Not Found"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"              
        403:
          description: "DENIED"
          headers:
            X-CorrelationID:
              type: string
              description: "Access Denied"
        405:
          description: "HTTP method is not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            X-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
            Allow:
              type: string
              description : "Allowed HTTP Methods"
        500:
          description: "Internal Error in Axway"
  /care/v1.0/useraccount:
    put:
      description: |
        Updates or Creates Secret Questions
      consumes:
        - application/json  
      produces:
        - application/json
      parameters:
        - name: X-appVersion
          in: header
          description: This is used to query the database.
          required: true
          type: string
        - name: X-apiKey
          in: header
          description: VAPI Key specific to the consumer, for e.g. kaisermobil93908784817875726966
          required: true
          type: string
        - name: X-appName
          in: header
          description: Application name i.e. kp‐mobile_v2.0, KP Mobile
          required: true
          type: string
        - name: X-useragenttype
          in: header
          description: Type of agent. For example iPhone5, Samsung etc.
          required: true
          type: string
        - name: X-useragentcategory
          in: header
          description: I = iOS, A = Android, M = mobile web
          required: true
          type: string
        - name: X-osversion
          in: header
          description: Version of the device OS i.e. 5.1
          required: true
          type: string
        - name: ssosession
          in: header
          description: ssosession header received from sign-on service. Required for authorization, Either ssoseesion header or ObSSOCookie is required
          required: true
          type: string
        - name: Set-Cookie
          in: header
          description: Interrupt response send set cookie same cookie has to pass here.
          required: true
          type: string
        - name: UpdateSecretQuestions
          in: body
          description: Secret Questions Update or Create 
          required: true
          schema:
            $ref: '#/definitions/New Secret Questions'          
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/New Secret Questions Resp"
        400:
          description: "Bad Request"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        401:
          description: "Unauthorized"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        412:
          description: "Precondition Failed"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
        503:
          description: "Referenced Backend Service is not available/System Error"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            x-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
          schema:
                $ref : "#/definitions/503ErrorRespSchema"              

        404:
          description: "Resource Not Found"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"              
        403:
          description: "DENIED"
          headers:
            X-CorrelationID:
              type: string
              description: "Access Denied"
        405:
          description: "HTTP method is not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            X-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
            Allow:
              type: string
              description : "Allowed HTTP Methods"
        500:
          description: "Internal Error in Axway"
definitions:
  Secret Questions:  
      type: object
      properties:
        '1':
          type: array
          items:
            type: object
            properties:
              questionId:
                type: string
                default: '13'
              groupId:
                type: string
                default: '1'
              text:
                type: string
                default: What is your father's middle name?
              textSp:
                type: string
                default: "¿Cuál es el segundo nombre de su padre?"
              answer:
                type: 'null'
                default: 
            required:
            - questionId
            - groupId
            - text
            - textSp
            - answer
        '2':
          type: array
          items:
            type: object
            properties:
              questionId:
                type: string
                default: '9'
              groupId:
                type: string
                default: '2'
              text:
                type: string
                default: When you were a child, what company did your father work for?
              textSp:
                type: string
                default: "¿En qué empresa trabajaba su padre cuando usted era niño?"
              answer:
                type: 'null'
                default: 
            required:
            - questionId
            - groupId
            - text
            - textSp
            - answer
        '3':
          type: array
          items:
            type: object
            properties:
              questionId:
                type: string
                default: '13'
              groupId:
                type: string
                default: '3'
              text:
                type: string
                default: What is your youngest sibling's first name?
              textSp:
                type: string
                default: "¿Cuál es el primer nombre de su hermano o hermana menor?"
              answer:
                type: 'null'
                default: 
            required:
            - questionId
            - groupId
            - text
            - textSp
            - answer
      required:
      - '1'
      - '2'
      - '3'

  New Secret Questions:
    type: object
    properties:
      secretQuestions:
        type: array
        items:
          type: object
          properties:
            questionId:
              type: string
              default: '6'
            groupId:
              type: string
              default: '1'
            text:
              type: 'null'
              default: null
            answer:
              type: string
              default: 'bbbb'
          required:
          - questionId
          - groupId
          - text
          - answer
    required:
    - secretQuestions
  New Secret Questions Resp:
      type: object
      properties:
        output:
          type: object
          properties:
            update:
              type: string
              default: success
          required:
          - update
        interruptList:
          type: string
          default: 'null'
        userInfo:
          type: object
          properties:
            email:
              type: string
              default: test@kp.org
            epicEmail:
              type: string
              default: test@kp.org
            mobilePhone:
              type: 'null'
              default: 
            ebizAccountRoles:
              type: array
              items:
                type: string
                default: MBR
          required:
          - email
          - epicEmail
          - mobilePhone
          - ebizAccountRoles
      required:
      - output
      - interruptList
      - userInfo

  503ErrorRespSchema:
        type: object
        properties:
          executionContext:
            type: object
            properties:
              errors:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              warnings:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              infos:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
            required:
            - errors
            - warnings
            - infos
        required:
        - executionContext
    