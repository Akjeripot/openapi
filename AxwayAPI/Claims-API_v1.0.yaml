swagger: "2.0"
info:
  version: 1.0.0
  title: Claims API
  description: The Claim Detail and Summary Service Consumer API RESTful web service is a secure service requiring an authentication token. This service API calls the KPCC claimDetailsV3 Service. The service provides the functionality to get claim details for given claimID and claim summary for given patient. 
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "API Wiki Link"
  url : "http://wiki-tech.kp.org/mediawiki/index.php/Claim_Detail_API
         http://wiki-tech.kp.org/mediawiki/index.php/Claim_Summary_API"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /mycare/v1.0
produces:
  - application/json
paths:
  /claimdetail:
    get:
      description: |
           Returns claim details for provided claim id.
      operationId: claimdetail
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: Accept
          in: header
          description: Application/json
          required: true
          type: string
        - name: X-inclusionJsonPath
          in: header
          description: If client pass this value, Axway Response global policy will use it and sends only specified keys
          required: false
          type: string
        - name: X-retainJsonSchema
          in: header
          description: This should be passed when X-inclusionJsonPath header is passed. If client pass ‘true’ value, then global path filter will not change json schema and send like whatever mentioned in this document, otherwise global filter change the json as per X-inclusionJsonPath keys
          required: false
          type: string
        - name: X-serviceStartDate
          in: header
          description: (Date format for this header value - yyyy-mm-dd). This date will value is required, when client needs to validate if a claimID belongs to claims information on or after specific date.
          required: false
          type: string
        - name: X-serviceEndDate
          in: header
          description: (Date format for this header value - yyyy-mm-dd). This date will value is required, when client needs to validate if a claimID belongs to claims information on or before specific date.
          required: false
          type: string
        - name: claimID
          in: query
          description: claim  id to get the claim details
          default: '998'
          required: true
          type: string
        - name: relID
          in: query
          description: Proxy relationId
          default: '123'
          required: false
          type: string

      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/GetClaimDetailResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'

  /claimsummary:
    get:
      description: |
           Returns claim summary for given patient.
      operationId: claimsummary
      parameters:
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
        - name: Accept
          in: header
          description: Application/json
          required: true
          type: string
        - name: X-USERID
          in: header
          description: It is optional header field, if client pass this value, Axway will use it, if not userId field value will be “Unknown”
          required: false
          type: string
        - name: X-inclusionJsonPath
          in: header
          description: If client pass this value, Axway Response global policy will use it and sends only specified keys
          required: false
          type: string
        - name: X-retainJsonSchema
          in: header
          description: This should be passed when X-inclusionJsonPath header is passed. If client pass ‘true’ value, then global path filter will not change json schema and send like whatever mentioned in this document, otherwise global filter change the json as per X-inclusionJsonPath keys
          required: false
          type: string
        - name: X-serviceStartDate
          in: header
          description: (Date format for this header value - yyyy-mm-dd). This date will value is required, when client needs claims from specific date. Note - If this header is not passed and X-serviceEndDate, then X-serviceStartDate value will be considered same as X-serviceEndDate
          required: false
          type: string
        - name: X-serviceEndDate
          in: header
          description: (Date format for this header value - yyyy-mm-dd). This date will value is required, when client needs claims to the specific date. Note -If this header is not passed and X-serviceStartDate, then its value is considered as current Date.
          required: false
          type: string
        - name: relID
          in: query
          description: Proxy relationId
          default: '123'
          required: false
          type: string

      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/GetClaimSummaryResponse"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
          
definitions:
    GetClaimDetailResponse:
          type: object
          properties:
            getClaimDetailResponse:
              type: object
              properties:
                patient:
                  type: object
                  properties:
                    claim:
                      type: object
                      properties:
                        recievedDate:
                          type: string
                          default: '2015-05-21'
                        claimID:
                          type: string
                          default: '998'
                        processStatus:
                          type: string
                          default: Processed
                        transaction:
                          type: array
                          items:
                            type: object
                            properties:
                              deductible:
                                type: string
                                default: '0.00'
                              allowedAmount:
                                type: string
                                default: '0.00'
                              NetPayable:
                                type: string
                                default: '0.00'
                              coinsuranceAmount:
                                type: string
                                default: '0.00'
                              " patFriendlyServiceDesc":
                                type: string
                                default: " Service Charge"
                              totalBilledAmount:
                                type: string
                                default: '2000.00'
                              transEOBCode:
                                type: array
                                items:
                                  type: object
                                  properties:
                                    EOBDate:
                                      type: 'null'
                                      default: 
                                    EOBComment:
                                      type: 'null'
                                      default: 
                                    EOBCodeDesc:
                                      type: string
                                      default: INFO,  OBSERVATION
                                    EOBCode:
                                      type: string
                                      default: '1643000051'
                                  required:
                                  - EOBDate
                                  - EOBComment
                                  - EOBCodeDesc
                                  - EOBCode
                              copay:
                                type: string
                                default: '0.00'
                              notCoveredAmount:
                                type: string
                                default: ''
                            required:
                            - deductible
                            - allowedAmount
                            - NetPayable
                            - coinsuranceAmount
                            - " patFriendlyServiceDesc"
                            - totalBilledAmount
                            - transEOBCode
                            - copay
                            - notCoveredAmount
                        amountOwedByMember:
                          type: string
                          default: '0'
                        serviceFromDate:
                          type: string
                          default: '2015-05-21'
                        amountApproved:
                          type: string
                          default: '0'
                        placeOfService:
                          type: string
                          default: LINDA MAR CARE CENTER-SNF
                        netPayableAmount:
                          type: string
                          default: '0'
                        check:
                          type: array
                          items:
                            type: object
                            properties:
                              number:
                                type: string
                                default: '2000000Z44'
                              issueDate:
                                type: string
                                default: '2015-05-21'
                            required:
                            - number
                            - issueDate
                        amountBilled:
                          type: string
                          default: '2000'
                        provider:
                          type: object
                          properties:
                            name:
                              type: string
                              default: LINDA MAR CARE CENTER
                          required:
                          - name
                      required:
                      - recievedDate
                      - claimID
                      - processStatus
                      - transaction
                      - amountOwedByMember
                      - serviceFromDate
                      - amountApproved
                      - placeOfService
                      - netPayableAmount
                      - check
                      - amountBilled
                      - provider
                  required:
                  - claim
              required:
              - patient
          required:
          - getClaimDetailResponse

    GetClaimSummaryResponse:
          type: object
          properties:
            getClaimSummaryResponse:
              type: object
              properties:
                patient:
                  type: array
                  items:
                    type: object
                    properties:
                      claim:
                        type: array
                        items:
                          type: object
                          properties:
                            recievedDate:
                              type: string
                              default: '2015-05-20'
                            claimID:
                              type: string
                              default: '997'
                            processStatus:
                              type: string
                              default: In Progress
                            serviceFromDate:
                              type: string
                              default: '2015-05-20'
                            provider:
                              type: object
                              properties:
                                name:
                                  type: string
                                  default: LINDA MAR CARE CENTER
                              required:
                              - name
                          required:
                          - recievedDate
                          - claimID
                          - processStatus
                          - serviceFromDate
                          - provider
                    required:
                    - claim
              required:
              - patient
          required:
          - getClaimSummaryResponse

    412ErrorRespSchema:
        type: object
        properties:
          errorCode:
            type: integer
            default: 1004
          errorMsg:
            type: string
            default: Claim ID is Required
        required:
        - errorCode
        - errorMsg

    503ErrorRespSchema:
        type: object
        properties:
          executionContext:
            type: object
            properties:
              errors:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              warnings:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
              infos:
                type: array
                items:
                  type: object
                  properties:
                    system:
                      type: string
                    message:
                      type: string
                    code:
                      type: integer
                  required:
                  - system
                  - message
                  - code
            required:
            - errors
            - warnings
            - infos
        required:
        - executionContext

parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: Device OS Version. For example 5.1. 
    default: 5.1.
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
    
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  405Error:
      description: "Method not supported"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
        $ref : "#/definitions/412ErrorRespSchema"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"