swagger: '2.0'
info:
  version: "1.0"
  title: HEC Facility Bookinginfo API
  description : " This API will return Health Education classes details for facility. By default this operation will return all facilities for NCAL (MRN)region but additional filter can be passed in request header like particular facility or clinic id or HEC name."
  contact:
    name: DSS API Services Team
    email: APISERVICES_TEAM@domino.kp.org
externalDocs:
  description: "`API Wiki Link`"
  url : "None"
schemes:
  - https
host: xjzxqws011fx.dta.kp.org:8080
basePath: /
consumes:
  - application/json
produces:
  - application/json
# Describe your paths here
paths:
  /mycare/v1.0/hecfacilitybookingInfo:
    get:
      description: |
        Returns HEC Facility Booking Info.
      produces:
        - application/json
      parameters:
        - name: X-facilityID
          in: header
          description: Facility Id, For e.g. SSF
          required: false
          type: string
        - name: X-clinicID
          in: header
          description: Clinic Id, For e.g. MED
          required: false
          type: string
        - name: X-hecShortName
          in: header
          description: HEC Short Name, For e.g. RISKHE
          required: false
          type: string
        - name: X-appVersion
          in: header
          description: This is used to query the database.
          required: true
          type: string
        - $ref: '#/parameters/UserAgentCategory'
        - $ref: '#/parameters/OSVersion'
        - $ref: '#/parameters/APIKey'
        - $ref: '#/parameters/UserAgentType'
        - $ref: '#/parameters/AppName'
        - $ref: '#/parameters/SSOSession'
        - $ref: '#/parameters/ObSSOCookie'
      responses:
        200:
          description: Success
          headers:
            X-CorrelationID:
              type: string
              description: "Correlation ID for the transaction"
            ssosession:
              type: string
              description: "SSO token"
          schema:
            $ref : "#/definitions/GetHECFacilityBookingDetailInfo"
        400:
          $ref: '#/responses/400Error'
        401:
          $ref: '#/responses/401Error'
        403:
          $ref: '#/responses/403Error'
        404:
          $ref: '#/responses/404Error'
        405:
          $ref: '#/responses/405Error'  
        412:
          $ref: '#/responses/412Error'
        500:
          $ref: '#/responses/500Error'
        503:
          $ref: '#/responses/503Error'
definitions:
  GetHECFacilityBookingDetailInfo:
      type: object
      properties:
        GetHECFacilityBookingDetailInfoResponse:
          type: array
          items:
            type: object
            properties:
              region:
                type: string
                default: MRN
              clinicID:
                type: string
                default: MED
              regionSelfServiceStatusFlag:
                type: string
                default: Y
              title:
                type: string
                default: Pre-Diabetes and You
              shortName:
                type: string
                default: RISKHE
              longName:
                type: string
                default: TBD
              description:
                type: string
                default: Learn how to delay - or even prevent - diabetes. This in-person
                  class gives you the information and tools to create your personal prediabetes
                  plan
              photoURL:
                type: string
                default: "/static/health/images/health_classes/hc_leftside.jpg"
              contentAssetId:
                type: string
                default: B432655A-DB98-11E3-AF85-C032CBE1731B
              resourceMnemonic:
                type: string
                default: RISKHE
              appointmentType:
                type: string
                default: RIS
              numberOfSession:
                type: integer
                default: 1
              genderRestrictionFlag:
                type: string
                default: N
              minAgeRestrictionFlag:
                type: string
                default: '0'
              feeRequiredFlag:
                type: string
                default: N
              feeMember:
                type: string
                default: '0'
              feeNonMember:
                type: string
                default: '0'
              feeMemberCopayReqFlag:
                type: string
                default: N
              selfServicePRMTNFlag:
                type: string
                default: Y
              selfServicePRMTNOrder:
                type: integer
                default: 5
              referralReqFlag:
                type: string
                default: N
              nonMemberAccessFlag:
                type: string
                default: Y
              publicAccessFlag:
                type: string
                default: Y
              programType:
                type: string
                default: CLASS
              HECFactDept:
                type: array
                items:
                  type: object
                  properties:
                    facilityId:
                      type: string
                      default: SSC
                    clinicID:
                      type: string
                      default: HED
                    facHecSBKNGStatus:
                      type: string
                      default: Y
                    deptHecSBKNGStatus:
                      type: string
                      default: Y
                    skipQSTNFlag:
                      type: string
                      default: Y
                    apptslotSrchMthdCd:
                      type: string
                      default: T
                    activityCode:
                      type: string
                      default: RIS
                    bookingGuideLineId:
                      type: string
                      default: HECDIABETES
                    resourceMnemonic:
                      type: string
                      default: RISKHE
                  required:
                  - facilityId
                  - clinicID
                  - facHecSBKNGStatus
                  - deptHecSBKNGStatus
                  - skipQSTNFlag
                  - apptslotSrchMthdCd
                  - activityCode
                  - bookingGuideLineId
                  - resourceMnemonic
            required:
            - region
            - clinicID
            - regionSelfServiceStatusFlag
            - title
            - shortName
            - longName
            - description
            - photoURL
            - contentAssetId
            - resourceMnemonic
            - appointmentType
            - numberOfSession
            - genderRestrictionFlag
            - minAgeRestrictionFlag
            - feeRequiredFlag
            - feeMember
            - feeNonMember
            - feeMemberCopayReqFlag
            - selfServicePRMTNFlag
            - selfServicePRMTNOrder
            - referralReqFlag
            - nonMemberAccessFlag
            - publicAccessFlag
            - programType
            - HECFactDept
      required:
      - GetHECFacilityBookingDetailInfoResponse
      
  412ErrorRespSchema:      
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors      
  503ErrorRespSchema:
      type: object
      properties:
        errors:
          type: array
          items:
            type: object
            properties:
              desc:
                type: string
              type:
                type: string
            required:
            - desc
            - type
      required:
      - errors
parameters:
  UserAgentCategory:
    name: X-useragentcategory
    in: header
    description: User Agent Category. Supported values I for iOS, A for Android, M for mobile web.
    default: I
    required: true
    type: string
  OSVersion:
    name: X-osversion
    in: header
    description: Device OS Version. For example 5.1. 
    default: 5.1.
    required: true
    type: string
  APIKey:
    name: X-apiKey
    in: header
    description:  API key/Client ID/APP ID  is required  to access this API . API key is  obtained by registering the application in API connect.  Follow link  for API key  registration https://kp.box.com/s/f55mko9rllosf9wehlkufjrhovkvmj6w
    default: kprwd65766367497853935616
    required: true
    type: string
  UserAgentType:
    name: X-useragenttype
    in: header
    description: Type of device for thick clients or user agent type in case of browser based Responsive Web Apps. For example - iPhone5, Samsung etc.
    default: iphone4
    required: true
    type: string
  AppName:
    name: X-appName
    in: header
    description: Name of the Application. For example KP Mobile.
    default: RWD
    required: true
    type: string
  SSOSession:
    name: ssosession
    in: header
    description: Valid OAM token received from SignOn API. Either ssosession or ObSSOCookie is mandatory
    default: CmlwKI81uwSS/BmJCgu88JBFaGUB4AEkXb0v7ekI6Vzbi6wuwzQg9+iFVcCwujNwlqslCPy8/Fg8+ZCCYFvSp2ycM4im1kV/pcz+H3qVzTYAEQIuJI608byefhRwOLNtj7a/2D48LVPKtDm9TZmU5KVmsyL9+Q3/TiNFYWCGNiFpTxOnKK4eb46nyf8j1mnvcqJpfB5S1App1S/x0ugX5i4jqNA79kjc2Hb/IXhpsO9M5P34s4oCoGHGLAUvB/7SkGidEQqWOoFLiwPhFihX0oHavMD3uPVgdQkSrvgO5XMtsbE7XGxoMVmdPTLrWEogbr+IUOvcagiQGLxP3vfE7ac52RhmI6RO0o1EiowANAg7FT1sTna90EttW6/8ByVDCjPzvi+uaZoWVNoknmf3i2/5abhD3Hi8KCyg8RnEFt4kw8HxezsJZvF/6XI+mXJh
    required: true
    type: string
  ObSSOCookie:
    name: ObSSOCookie
    in: header
    description: Either ObSSOCookie or ssosession is required. If portal or RWD app is using OAM directly than Signon API to authenticate then send ObSSOCookie
    required: false
    type: string
responses:
  400Error:
      description: "Bad Request"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  401Error:
      description: "Unauthorized"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  403Error:
      description: "Method not supported or Incorrect URI."
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  404Error:
      description: "Resource not found"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  405Error:
          description: "HTTP method is not supported"
          headers:
            X-CorrelationID:
              type: string
              description: "Unique Correlation ID for the transaction"
            X-appName:
              type: string
              description:  "Application Name"
            X-osversion:
              type: string
              description:  "OS Version"
            X-useragentcategory:
              type: string
              description: "User Agent Category"
            X-useragenttype:
              type: string
              description: "User Agent type"
            ssosession:
              type: string
              description: "SSO Token"
            Allow:
              type: string
              description : "Allowed HTTP Methods"
  412Error:
      description: "Precondition failed.  One or more input param/header values are incorrect or not supported."
      schema:
          $ref : "#/definitions/412ErrorRespSchema"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
  500Error:
      description: "Internal Server Error"
  503Error:
      description: "Service Unavailable (When backend systems/services are not available, backend error)"
      headers:
        X-CorrelationID:
          type: string
          description: "Unique Correlation ID for the transaction"
        X-appName:
          type: string
          description:  "Application Name"
        X-osversion:
          type: string
          description:  "OS Version"
        x-useragentcategory:
          type: string
          description: "User Agent Category"
        X-useragenttype:
          type: string
          description: "User Agent type"
        ssosession:
          type: string
          description: "SSO Token"
      schema:
            $ref : "#/definitions/503ErrorRespSchema"
